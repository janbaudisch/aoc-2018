use std::cmp::Ordering;

#[derive(Debug, Eq, PartialEq)]
pub struct Step {
    pub id: char,
    pub dependencies: Vec<char>
}

impl Step {
    pub fn new(id: char) -> Self {
        Step {
            id,
            dependencies: Vec::new()
        }
    }
}

// make step able to be ordered
impl Ord for Step {
    fn cmp(&self, other: &Step) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

// make step able to be partially ordered
impl PartialOrd for Step {
    fn partial_cmp(&self, other: &Step) -> Option<Ordering> {
        if self.dependencies.len() == 0 && other.dependencies.len() == 0 {
            if self.id > other.id {
                return Some(Ordering::Greater);
            }

            return Some(Ordering::Less);
        }

        if self.dependencies.len() == 0 {
            return Some(Ordering::Less);
        }

        if other.dependencies.len() == 0 {
            return Some(Ordering::Greater);
        }

        if self.dependencies.contains(&other.id) {
            return Some(Ordering::Greater);
        }

        if other.dependencies.contains(&self.id) {
            return Some(Ordering::Less);
        }

        if self.id > other.id {
            return Some(Ordering::Greater);
        }

        Some(Ordering::Less)
    }
}
