use std::io;

pub fn read_line() -> String {
    let mut input = String::new();

    // read from stdin
    match io::stdin().read_line(&mut input) {
        Ok(_) => {
            // remove newline
            input.pop();
        }
        Err(error) => panic!("{}", error)
    }

    input
}

pub fn read_lines() -> Vec<String> {
    let mut lines: Vec<String> = Vec::new();

    // start input loop
    loop {
        let mut input = String::new();

        // read from stdin
        match io::stdin().read_line(&mut input) {
            Ok(_) => {
                // stop input loop on enter / newline
                if input == "\n" {
                    break;
                }

                // remove newline
                input.pop();

                lines.push(input.clone());
            }
            Err(error) => panic!("{}", error)
        }
    }

    lines
}
