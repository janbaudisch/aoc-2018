mod step;

use common::input;
use std::collections::HashMap;
use crate::step::Step;

fn main() {
    let mut steps: HashMap<char, Step> = HashMap::new();

    // convert input
    for mut input in input::read_lines() {
        input.truncate(37);
        let id = input.pop().unwrap();
        input.truncate(6);
        let dependency = input.pop().unwrap();
        steps.entry(id).or_insert(Step::new(id)).dependencies.push(dependency);
        steps.entry(dependency).or_insert(Step::new(dependency));
    }

    // part one: find correct order of steps
    let mut order: Vec<Step> = steps.into_iter().map(|(_, step)| step).collect();
    order.sort_unstable();

    print!("[PART ONE] correct order: ");

    for step in order {
        print!("{}", step.id);
    }

    println!();
}
