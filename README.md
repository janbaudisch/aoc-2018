[![Build][build-img]][build-url]

# Advent of Code 2018

> My Rust implementation of Advent of Code 2018

[build-img]: https://travis-ci.com/flyingP0tat0/aoc-2018.svg?branch=master
[build-url]: https://travis-ci.com/flyingP0tat0/aoc-2018
