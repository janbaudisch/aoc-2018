#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Guard {
    pub id: u16
}

impl Guard {
    pub fn new(id: u16) -> Guard {
        Guard {
            id
        }
    }
}
