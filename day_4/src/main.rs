mod guard;
mod log;

use common::input;
use crate::guard::Guard;
use crate::log::{Event, Log};
use std::collections::HashMap;

fn main() {
    // get input and parse logs
    let mut logs: Vec<Log> = input::read_lines()
        .iter()
        .map(|x| Log::from(x.to_string()))
        .collect();
    
    // sort logs
    logs.sort_unstable();

    let mut guards: HashMap<Guard, Vec<u8>> = HashMap::new();
    let mut current = Guard::new(0);
    let mut sleep_started = 0;

    // strategy one: choose guard with most sleep time
    for log in logs {
        match log.event {
            Event::BeginShift(guard) => current = guard,
            Event::FallAsleep => sleep_started = log.date.minute,
            Event::WakeUp => {
                let mut asleep = Vec::new();

                for x in sleep_started..log.date.minute {
                    asleep.push(x);
                }

                guards.entry(current).or_insert(Vec::new()).append(&mut asleep);
            }
        }
    }

    let (mut most, _) = guards.iter().next().unwrap();

    // find guard most time asleep
    for (guard, asleep) in guards.iter() {
        if asleep.len() > guards.get(&most).unwrap().len() {
            most = guard;
        }
    }

    let mut minutes: HashMap<&u8, u8> = HashMap::new();

    for minute in guards.get(&most).unwrap() {
        *minutes.entry(minute).or_insert(0) += 1;
    }

    let mut most_minute = 0;
    let mut most_recurrence = 0;

    // find minute guard is asleep most of the time
    for (minute, recurrence) in minutes.iter() {
        if recurrence > &most_recurrence {
            most_minute = **minute;
            most_recurrence = *recurrence;
        }
    }

    // part one: guard ID * minute asleep most of the time
    println!("[PART ONE] {}", most.id as u32 * most_minute as u32);
}
