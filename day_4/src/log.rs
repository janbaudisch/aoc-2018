use std::cmp::Ordering;
use super::Guard;

#[derive(Eq, PartialEq)]
pub struct Log {
    pub date: Date,
    pub event: Event
}

#[derive(Eq, PartialEq)]
pub struct Date {
    pub year: u16,
    pub month: u8,
    pub day: u8,
    pub hour: u8,
    pub minute: u8
}

#[derive(Eq, PartialEq)]
pub enum Event {
    BeginShift(Guard),
    FallAsleep,
    WakeUp
}

// convert string to log
impl From<String> for Log {
    fn from(string: String) -> Self {
        let mut progress: u8 = 0;
        let mut year = String::new();
        let mut month = String::new();
        let mut day = String::new();
        let mut hour = String::new();
        let mut minute = String::new();
        let mut action = String::new();

        for character in string.chars() {
            match character {
                '[' | ']' => continue,
                '-' | ':' | ' ' => progress += 1,
                _ => {
                    match progress {
                        0 => year.push(character),
                        1 => month.push(character),
                        2 => day.push(character),
                        3 => hour.push(character),
                        4 => minute.push(character),
                        _ => action.push(character)
                    }
                }
            }
        }

        Log {
            date: Date {
                year: u16::from_str_radix(&year, 10).expect("error converting input"),
                month: u8::from_str_radix(&month, 10).expect("error converting input"),
                day: u8::from_str_radix(&day, 10).expect("error converting input"),
                hour: u8::from_str_radix(&hour, 10).expect("error converting input"),
                minute: u8::from_str_radix(&minute, 10).expect("error converting input")
            },
            event: Event::from(action)
        }
    }
}

// make log able to be ordered
impl Ord for Log {
    fn cmp(&self, other: &Log) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

// make log able to be partially ordered
impl PartialOrd for Log {
    fn partial_cmp(&self, other: &Log) -> Option<Ordering> {
        self.date.partial_cmp(&other.date)
    }
}

// make date able to be partially ordered
impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Date) -> Option<Ordering> {
        if self == other {
            return Some(Ordering::Equal);
        }

        if self.year > other.year {
            return Some(Ordering::Greater);
        }

        if self.year < other.year {
            return Some(Ordering::Less);
        }

        if self.month > other.month {
            return Some(Ordering::Greater);
        }

        if self.month < other.month {
            return Some(Ordering::Less);
        }

        if self.day > other.day {
            return Some(Ordering::Greater);
        }

        if self.day < other.day {
            return Some(Ordering::Less);
        }

        if self.hour > other.hour {
            return Some(Ordering::Greater);
        }

        if self.hour < other.hour {
            return Some(Ordering::Less);
        }

        if self.minute > other.minute {
            return Some(Ordering::Greater);
        }

        Some(Ordering::Less)
    }
}

// convert string to action
impl From<String> for Event {
    fn from(string: String) -> Self {
        match string.as_str() {
            "fallsasleep" => Event::FallAsleep,
            "wakesup" => Event::WakeUp,
            _ => {
                let mut id = string;
                id.retain(|x| x.is_digit(10));
                let guard = Guard::new(u16::from_str_radix(&id, 10).expect("error converting input"));
                Event::BeginShift(guard)
            }
        }
    }
}
