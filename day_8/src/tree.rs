pub struct Tree(Vec<Node>);

pub struct Node {
    pub id: u8,
    pub children: Vec<Box<Node>>,
    pub metadata: Vec<u8>
}

// convert string to tree
impl From<String> for Tree {
    fn from(string: String) -> Self {
        let mut nodes = Vec::new();
        Tree(nodes)
    }
}
