mod coordinate;

use common::input;
use crate::coordinate::Coordinate;

fn main() {
    // get input and parse coordinates
    let coordinates: Vec<Coordinate> = input::read_lines()
        .iter()
        .map(|x| Coordinate::from(x.to_string()))
        .collect();
}
