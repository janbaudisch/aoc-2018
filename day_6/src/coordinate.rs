#[derive(Clone, Copy)]
pub struct Coordinate((u8, u8));

// convert string to coordinate
impl From<String> for Coordinate {
    fn from(string: String) -> Self {
        let coordinate: Vec<&str> = string.split(", ").collect();
        let x = u8::from_str_radix(coordinate[0], 10).expect("error converting input");
        let y = u8::from_str_radix(coordinate[1], 10).expect("error converting input");
        Coordinate((x, y))
    }
}
