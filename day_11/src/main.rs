mod cell;

use crate::cell::Cell;
use common::input;

fn main() {
    let serial_number = u32::from_str_radix(input::read_line().as_str(), 10).expect("Could not parse serial number!");

    let mut grid: Vec<Vec<Cell>> = Vec::new();

    for x in 1..301 {
        let mut column: Vec<Cell> = Vec::new();

        for y in 1..301 {
            column.push(Cell::new(x, y));
        }

        grid.push(column);
    }

    let mut max: i32 = 0;
    let mut max_start = (1, 1);

    for x in 1..298 {
        for y in 1..298 {
            let mut combined: i32 = 0;

            for x_offset in 0..3 {
                for y_offset in 0..3 {
                    let cell = grid[x + x_offset - 1][y + y_offset - 1];
                    combined += cell.calculate_power(&serial_number);
                }

                if combined > max {
                    max = combined;
                    max_start = (x, y);
                }
            }
        }
    }

    let (max_x, max_y) = max_start;
    println!("[PART ONE] starting point of 3x3 square with most power: ({}, {})", max_x, max_y);

    // TODO: takes way too long

    let mut max: i32 = 0;
    let mut max_identifier = (1, 1, 1);

    for x in 1..298 {
        for y in 1..298 {
            let mut combined: i32 = 0;

            let max_i = if x > y {
                301 - x
            } else {
                301 - y
            };

            for i in 1..max_i {
                for x_offset in 0..i {
                    for y_offset in 0..i {
                        let cell = grid[x + x_offset - 1][y + y_offset - 1];
                        combined += cell.calculate_power(&serial_number);
                    }

                    if combined > max {
                        max = combined;
                        max_identifier = (x, y, i);
                    }
                }
            }
        }
    }

    let (max_x, max_y, max_size) = max_identifier;
    println!("[PART TWO] (X, Y, size) identifier for square with most power: ({}, {}, {})", max_x, max_y, max_size);
}
