#[derive(Clone, Copy)]
pub struct Cell {
    x: u16,
    y: u16
}

impl Cell {
    pub fn new(x: u16, y: u16) -> Self {
        Cell {
            x,
            y
        }
    }

    pub fn calculate_power(&self, serial_number: &u32) -> i32 {
        let rack_id = self.x as u32 + 10;
        ((rack_id * self.y as u32 + serial_number) * rack_id).extract(2) as i32 - 5
    }
}

trait Extract {
    fn extract(&self, offset: usize) -> Self;
}

impl Extract for u32 {
    fn extract(&self, offset: usize) -> Self {
        let mut string = self.to_string();

        if offset > string.len() {
            return 0;
        }

        let new_length = &string.len() - offset;
        string.truncate(new_length);
        string.pop().unwrap().to_digit(10).unwrap()
    }
}
